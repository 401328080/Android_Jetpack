package com.study.jetpack;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.common.lib.ui.BaseActivity;

import butterknife.BindView;

public class MainActivity extends BaseActivity {
    @BindView(R.id.rv_list)
    RecyclerView mRecyclerView;
    private TemplateAdapter mAdapter;

    @Override
    protected void onCreateProxy(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        init();
        setListeners();
    }

    @Override
    public void init() {
        super.init();
        mAdapter = new TemplateAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setNewInstance(TemplateDataUtils.getListInfos());
    }

    @Override
    public void setListeners() {
        super.setListeners();
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            TemplateListInfo item = (TemplateListInfo) adapter.getData().get(position);
            ContainerActivity.startContainerActivity(context, item.getFragment().getCanonicalName(), item.getTitle());
        });
    }
}
