package com.study.jetpack;


import com.study.jetpack.liveData.LiveDataFragment;
import com.study.jetpack.room.RoomFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Author dengdayi
 *
 * @date: 2020-04-03 12:10
 */
public class TemplateDataUtils {
    private static List<TemplateListInfo> listInfos;

    static {
        listInfos = new ArrayList<>();
        listInfos.add(new TemplateListInfo(RoomFragment.class, R.string.jetpack_room));
        listInfos.add(new TemplateListInfo(LiveDataFragment.class, R.string.jetpack_live_data));
    }

    public static List<TemplateListInfo> getListInfos() {
        return listInfos;
    }
}
