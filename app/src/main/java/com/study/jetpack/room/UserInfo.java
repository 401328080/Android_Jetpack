package com.study.jetpack.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * Created by Administrator
 * Date: 2020/04/09 15：43
 * Desc:
 */
@Entity
public class UserInfo {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "uuid")
    public int uid;
    @ColumnInfo(name = "user_name")
    public String userName;

    @Ignore
    public UserInfo() {
    }

    public UserInfo(Integer uid, String userName) {
        this.uid = uid;
        this.userName = userName;
    }
}

