package com.study.jetpack.room;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

/**
 * Created by Administrator
 * Date: 2020/04/11 11：57
 * Desc:地址
 */
@Entity(foreignKeys = @ForeignKey(entity = UserInfo.class, parentColumns = "uuid",
        childColumns = "uid"))
public class AddressInfo {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "address_id")
    public int id = 1001;
    @ColumnInfo(name = "province")
    public String province;
    @ColumnInfo(name = "city")
    public String city;
    @ColumnInfo(name = "county")
    public String county;
    @ColumnInfo(name = "address")
    public String address;
    @ColumnInfo(name = "uid")
    public int uid;
}
