package com.study.jetpack.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * Created by Administrator
 * Date: 2020/04/09 15：48
 * Desc:建议使用单例创建 DataBase
 */
@Database(entities = {UserInfo.class, SubjectInfo.class, AddressInfo.class}, version = 100)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserInfoDao userInfoDao();
}
