package com.study.jetpack.room;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.study.jetpack.R;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Administrator
 * Date: 2020/04/10 09：17
 * Desc:
 */
public class UserListAdapter extends BaseQuickAdapter<UserInfo, BaseViewHolder> {
    public UserListAdapter() {
        super(R.layout.recycler_item_text);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, UserInfo info) {
        String text = "ID:" + info.uid + " --->姓名:" + info.userName;
        baseViewHolder.setText(R.id.tv_text, text);
    }
}
