package com.study.jetpack.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Administrator
 * Date: 2020/04/11 11：56
 * Desc:科目
 */
@Entity
public class SubjectInfo {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "subject_id")
    public int subject_id=1001;
    @ColumnInfo(name = "subject_name")
    public String subject_name;
}
