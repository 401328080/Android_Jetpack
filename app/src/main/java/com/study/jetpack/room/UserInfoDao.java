package com.study.jetpack.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * Created by Administrator
 * Date: 2020/04/09 15：45
 * Desc:
 */
@Dao
public interface UserInfoDao {
    @Query("SELECT * FROM userinfo ORDER BY uuid ASC")
    List<UserInfo> getAll();

    //查单独的
    @Query("SELECT * FROM userinfo WHERE uuid = :number")
    UserInfo getUser(String number);

    //查数组
    @Query("SELECT * FROM userinfo WHERE uuid IN (:numbers)")
    List<UserInfo> getUsers(List<String> numbers);

    @Insert
    void insertAll(UserInfo... users);

    @Delete
    void delete(UserInfo user);

    //写语句需要 用 Query 处理
    @Query("UPDATE userinfo  SET user_name=:name WHERE uuid=:number")
    void update(String number, String name);

    //更新全部字段，需添加onConflict = OnConflictStrategy.REPLACE
    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(UserInfo info);
}
