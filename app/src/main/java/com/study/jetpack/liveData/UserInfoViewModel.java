package com.study.jetpack.liveData;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.study.jetpack.room.UserInfo;

import java.util.List;

/**
 * Created by Administrator
 * Date: 2020/04/13 10：22
 * Desc:LiveData 是一个抽象类，它的实现子类有 MutableLiveData ，MediatorLiveData。
 * 在实际使用中，用得比较多的是 MutableLiveData。他常常结合 ViewModel 一起使用。
 * 首先，我们先写一个类继承我们的 ViewModel，里面持有 mNameEvent。
 */
public class UserInfoViewModel extends ViewModel {
    private MutableLiveData<List<UserInfo>> userInfoData = new MutableLiveData<>();

    public MutableLiveData<List<UserInfo>> getUserInfoData() {
        return userInfoData;
    }
}
