package com.study.jetpack.liveData;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.common.lib.ui.BaseFragment;
import com.study.jetpack.JetpackApplication;
import com.study.jetpack.R;
import com.study.jetpack.room.UserInfo;
import com.study.jetpack.room.UserListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator
 * Date: 2020/04/13 10：21
 * Desc:LiveData 测试
 */
public class LiveDataFragment extends BaseFragment {
    @BindView(R.id.layout_mask)
    RelativeLayout layout_mask;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_number)
    EditText et_number;
    @BindView(R.id.rv_list)
    RecyclerView rv_list;
    private UserListAdapter mAdapter;
    private UserInfoViewModel userInfoViewModel;

    @Override
    protected void onCreateProxy(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        setContentView(R.layout.fragment_room);
    }

    @Override
    protected void onViewCreatedProxy(View view, Bundle bundle) {
        init();
        setListeners();
    }

    @Override
    public void init() {
        super.init();
        userInfoViewModel = ViewModelProviders.of(this).get(UserInfoViewModel.class);
        MutableLiveData<List<UserInfo>> userInfoData = userInfoViewModel.getUserInfoData();
        userInfoData.observe(this, userInfos -> mAdapter.setNewInstance(userInfos));

        rv_list.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new UserListAdapter();
        rv_list.setAdapter(mAdapter);
        requestList();
    }


    @Override
    public void setListeners() {
        super.setListeners();

        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            UserInfo userInfo = (UserInfo) adapter.getData().get(position);
            @SuppressLint("StaticFieldLeak")
            AsyncTask task = new AsyncTask<Object, Void, Void>() {
                @Override
                protected Void doInBackground(Object[] objects) {
                    JetpackApplication.getApp().getAppDatabase().userInfoDao().delete(userInfo);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    requestList();
                }
            };
            task.execute();
        });
    }

    @OnClick(R.id.tv_add)
    void onShowLayoutMask() {
        layout_mask.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tv_insert)
    void onInsert() {
        String number = et_number.getText().toString();
        String name = et_name.getText().toString();
        @SuppressLint("StaticFieldLeak")
        AsyncTask task = new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object[] objects) {
                UserInfo userInfo = new UserInfo(Integer.parseInt(number), name);
                UserInfo info = JetpackApplication.getApp().getAppDatabase().userInfoDao().getUser(number);
                if (info != null) {
                    JetpackApplication.getApp().getAppDatabase().userInfoDao().update(userInfo);
                } else {
                    JetpackApplication.getApp().getAppDatabase().userInfoDao().insertAll(userInfo);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                layout_mask.setVisibility(View.GONE);
                requestList();
            }
        };
        task.execute();
    }

    private void requestList() {
        @SuppressLint("StaticFieldLeak")
        AsyncTask task = new AsyncTask<Object, List<UserInfo>, List<UserInfo>>() {
            @Override
            protected List<UserInfo> doInBackground(Object[] objects) {
                List<UserInfo> userInfos = JetpackApplication.getApp().getAppDatabase().userInfoDao().getAll();
                return userInfos;
            }

            @Override
            protected void onPostExecute(List<UserInfo> userInfos) {
                super.onPostExecute(userInfos);
                userInfoViewModel.getUserInfoData().setValue(userInfos);
            }
        };
        task.execute();
    }

}
