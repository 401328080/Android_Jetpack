package com.study.jetpack;

import com.common.lib.base.IBaseInfo;
import com.common.lib.ui.BaseFragment;

/**
 * Author dengdayi
 *
 * @date: 2020-04-01 09:23
 */
public class TemplateListInfo<T extends BaseFragment> implements IBaseInfo {
    private Class<T> fragment;
    private int title;

    public TemplateListInfo(Class<T> fragment, int title) {
        this.fragment = fragment;
        this.title = title;
    }

    public Class<T> getFragment() {
        return fragment;
    }

    public void setFragment(Class<T> fragment) {
        this.fragment = fragment;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }
}
