package com.study.jetpack;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

/**
 * Author dengdayi
 *
 * @date: 2020-03-31 17:52
 */
public class TemplateAdapter extends BaseQuickAdapter<TemplateListInfo, BaseViewHolder> {
    public TemplateAdapter() {
        super(R.layout.recycler_item_template);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, TemplateListInfo templateListInfo) {
        baseViewHolder.setText(R.id.tv_item_text, templateListInfo.getTitle());
    }
}
